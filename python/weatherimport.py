# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 11:35:46 2019


"""
from epw import epw
import pandas as pd
import os
import numpy as np
import math
from pvlib import solarposition

# Find the path of the script
module_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def getTimebase():
    timebase = pd.date_range(start=pd.to_datetime('2005-01-01 00:00:00', format='%Y-%m-%d %H:%M:%S'),
                            end=pd.to_datetime('2005-12-31 23:00:00', format='%Y-%m-%d %H:%M:%S'),
                            freq='H')
    return timebase

def getRT2012(zone="H1a"):
    timebase = getTimebase()

    delta_t = 3600

    weather = os.path.join(module_path, 'data', 'in', 'weather_rt2012.xls')
    weather_df = pd.read_excel(weather, sheet_name=zone)
    #weather = weather[0:8760]
    
    weather_df['time'] = timebase
    weather_df = weather_df.set_index('time')
    weather_df = weather_df.resample(str(delta_t)+'s').interpolate(method='linear')
	#weather['Teau'] = (-0.5*np.cos((weather.index.dayofyear.values - 45)*2*np.pi/365))*10 + 13
    climeliothwea = pd.DataFrame(columns=["air_temperature",
                                       "water_temperature",
                                       "sky_temperature",
                                       "air_humidity",
                                       "direct_normal_irradiation",
                                       "diffuse_horizontal_irradiation",
                                       "solar_elevation",
                                       "solar_azimuth",
                                       "wind_speed"],
                                index = weather_df.index)
    
    climeliothwea.air_temperature = weather_df["te0"].values + 273.15
    climeliothwea.water_temperature = weather_df["Teau"] + 273.15
    climeliothwea.sky_temperature = weather_df["Teciel"].values + 273.15
    climeliothwea.air_humidity = weather_df['we0'].values/1000
    climeliothwea.direct_normal_irradiation = weather_df['dirN'].values
    climeliothwea.diffuse_horizontal_irradiation = weather_df['diff'].values
    climeliothwea.solar_elevation = weather_df['Gamma'].values*np.pi/180
    climeliothwea.solar_azimuth = weather_df['Psi'].values*np.pi/180
    climeliothwea.wind_speed = weather_df['Vent'].values
    
    climeliothwea.index.name = None
    return climeliothwea

def getEPWheaders(epwfile):
    epweather=epw()
    epweather.read(epwfile)
    return epweather.headers

def getEPW(epwfile):
    epweather=epw()
    epweather.read(epwfile)

    ## EnergyPlus Sky Temperature Calculation
    ## https://www.energyplus.net/sites/default/files/docs/site_v8.3.0/EngineeringReference/05-Climate/index.html
    epweather.dataframe["Sky Temperature"] = (epweather.dataframe["Horizontal Infrared Radiation Intensity"]/5.6697e-8)**0.25-273.15
    epweather.dataframe["Sky Temperature"]

    #Convert Relative Humidity to Absolute (g/m^3)
    epweather.dataframe["Absolute Humidity"] = (6.112 * math.e**((17.67 * epweather.dataframe["Dry Bulb Temperature"])/(epweather.dataframe["Dry Bulb Temperature"]+243.5)) * epweather.dataframe["Relative Humidity"] * 2.1674)/(273.15+epweather.dataframe["Dry Bulb Temperature"])
	#g/m^3 to g/kg
    epweather.dataframe["Absolute Humidity"] = epweather.dataframe["Absolute Humidity"] / 1.204 * epweather.dataframe["Atmospheric Station Pressure"]/101300*(20+273.15)/(epweather.dataframe["Dry Bulb Temperature"]+273.15)


    climeliothwea = pd.DataFrame(columns=["air_temperature",
                                       "water_temperature",
                                       "sky_temperature",
                                       "air_humidity",
                                       "direct_normal_irradiation",
                                       "diffuse_horizontal_irradiation",
                                       "solar_elevation",
                                       "solar_azimuth",
                                       "wind_speed"], index=getTimebase())
    lat, long = float(epweather.headers["LOCATION"][5]), float(epweather.headers["LOCATION"][6])
    altitude = float(epweather.headers["LOCATION"][8])

    solar = solarposition.get_solarposition(climeliothwea.index, lat, long, altitude)
    climeliothwea.air_temperature = epweather.dataframe["Dry Bulb Temperature"].values + 273.15
    climeliothwea.water_temperature = getRT2012()["water_temperature"].values #Use values from RT2012
    print('Warning: Using water temperature from RT2012 - H1a zone')
    climeliothwea.sky_temperature = epweather.dataframe["Sky Temperature"].values + 273.15
    climeliothwea.air_humidity = epweather.dataframe["Absolute Humidity"].values/1000
    climeliothwea.direct_normal_irradiation = epweather.dataframe["Direct Normal Radiation"].values
    climeliothwea.diffuse_horizontal_irradiation = epweather.dataframe["Diffuse Horizontal Radiation"].values
    climeliothwea.solar_elevation = solar["elevation"].values*np.pi/180 
    climeliothwea.solar_azimuth = solar["azimuth"].values*np.pi/180-np.pi
    climeliothwea.wind_speed = epweather.dataframe["Wind Speed"].values
    
    climeliothwea.index.name = None
    return climeliothwea
