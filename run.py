# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 13:12:20 2019


"""

import sys
import os
import getopt


class Logger():
	stdout = sys.stdout
	messages = []

	def start(self): 
		sys.stdout = self

	def stop(self): 
		sys.stdout = self.stdout

	def write(self, text): 
		self.messages.append(text)

	def empty(self):
		self.messages = []


def main(argv):
	# Find the path of the script
	script_path = os.path.abspath(__file__)
	dir_path = os.path.dirname(script_path)
	sys.path.append(dir_path + "/python/")
	import weatherimport
	
	output_csv = "weather.csv"
	weather = ""
	
	try:
		opts, args = getopt.getopt(argv, "w:o:", ["weather=", "output_csv="])
	except getopt.GetoptError:
		print('run.py -w <optional_weather_file_path_or_zone> -o <output_file_path>')
		sys.exit(2)
     
	for opt, arg in opts:
		if opt in ("-w", "--weather_file_path"):
			weather = arg
		elif opt in ("-o", "--output_file_path"):
			output_csv = arg
			
	#Start logging warnings		
	log = Logger()
	log.start()
	
	if weather == "": #default RT2012 zone
		climeliothwea = weatherimport.getRT2012()
		with open(output_csv, 'w') as f:
			f.write("# RT2012 weather data for default zone H1a\n")
	elif weather[-4:] == ".epw": #custom weather file
		climeliothwea = weatherimport.getEPW(weather)
		with open(output_csv, 'w') as f:
			f.write("# EPW weather data\n# Original file: {}\n# EPW headers: {}\n".format(weather,weatherimport.getEPWheaders(weather)))
	else: #custom RT2012 zone
		climeliothwea = weatherimport.getRT2012(weather)
		with open(output_csv, 'w') as f:
			f.write("# RT2012 weather data for zone {}\n".format(weather))
			
	log.stop()
	#Write Warnings to file
	with open(output_csv, 'a') as f:
		for message in log.messages[:-1]:
			f.write("# {}\n".format(message))
	climeliothwea.to_csv(output_csv,mode='a') 

if __name__ == "__main__":
	main(sys.argv[1:])